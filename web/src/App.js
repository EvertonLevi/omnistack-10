import React, { useEffect, useState } from 'react';
import api from './services/api'
import './global.css';
import './App.css';
import './sidebar.css';
import './main.css';
import DevItem from '../src/components/DevItem'
import DevForm from '../src/components/DevForm'


//Componente: isolar trechos da aplicação, parte q n atinja diretamente outras partes do aplcc
//Estado: informações mantidas pelo component (imutabilidade)
//Propriedade: atributo (ex: title:"") / infor q o pai passa para o filho
// usar <> + </> como flagment para n usar <div>s e atrapalhar na estilização

function App() {
	const [devs, setDevs] = useState([])




	useEffect(() => {
		async function loadDevs() {
			const response = await api.get('/devs')
			setDevs(response.data)
		}
		loadDevs()
	}, [])

	async function handleAddDev(data) {
		const response = await api.post('/dev', data)

		setDevs([...devs, response.data])
	}

	return (
		<div id="app">
			<aside>
				<strong>Cadastrar</strong>
				<DevForm onSubmit={handleAddDev} />
			</aside>
			<main>
				<ul>
					{devs.map(dev => (
						<DevItem key={dev._id} dev={dev} />
					))}
				</ul>
			</main>
		</div>

	);
}

export default App;
