const express = require('express');
const mongoose = require('mongoose')
const routes = require('./routes')

const app = express();
//lembrar de mudar <password> para o que escolhi nas configs do mongo DB
// lembrar de mudar /test?, pois vem como default "test" como o banco inicial
mongoose.connect('mongodb+srv://levi:levi@cluster0-4njdn.mongodb.net/week10?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})

app.use(express.json())
app.use(routes)

//Métodos http: get, put, post and delete

//tipos de param: 
//query params: request.query (filter, order, pages ...), 
//route params: request.params (identificar recurso na alteração, remoção),
// body : request.body (dados para criação ou alteração de algum registro)

app.listen(3333);

